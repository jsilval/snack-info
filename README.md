# SnackInfo

 Un fork de la SnackBar de android
 
 ![SnackInfo](assets/snackinfo.gif)
 
 
 ## Uso
 
```java
  SnackInfo.make(view, "Hello World!! ", SnackInfo.LENGTH_SHORT)
                        .setAction("Action", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Log.d("TAG", "onClick: action");
                            }
                        })
                        .addCallback(new BaseTransientBottomBar.BaseCallback<SnackInfo>() {
                            @Override
                            public void onDismissed(SnackInfo transientBottomBar, int event) {
                                Log.d("TAG", "onDismissed: ");
                            }

                            @Override
                            public void onShown(SnackInfo transientBottomBar) {
                                Log.d("TAG", "onShown: ");
                            }
                        })
                        .setBackgroundColor(Color.LTGRAY)
                        .setActionTextColor(Color.BLUE)
                        .setTextColor(Color.RED)
                        .animate(FROM, TO)
                        .setGravity(GRAVITY)
                        .show();
```

