package com.example.dsilva.snackinfo;

import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioGroup;

import co.jsilval.snackinfo.AnimationBar;
import co.jsilval.snackinfo.BaseTransientBottomBar;
import co.jsilval.snackinfo.SnackInfo;

public class MainActivity extends AppCompatActivity {
    private int FROM = AnimationBar.FROM_BOTTOM;
    private int TO = AnimationBar.TO_BOTTOM;

    private int GRAVITY = Gravity.BOTTOM;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        RadioGroup rgTo = findViewById(R.id.rgTo);
        RadioGroup rgFrom = findViewById(R.id.rgFrom);
        RadioGroup rgGravity = findViewById(R.id.rgGravity);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SnackInfo.make(view, "Hello World!! ", SnackInfo.LENGTH_SHORT)
                        .setAction("Action", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Log.d("TAG", "onClick: action");
                            }
                        })
                        .addCallback(new BaseTransientBottomBar.BaseCallback<SnackInfo>() {
                            @Override
                            public void onDismissed(SnackInfo transientBottomBar, int event) {
                                Log.d("TAG", "onDismissed: ");
                            }

                            @Override
                            public void onShown(SnackInfo transientBottomBar) {
                                Log.d("TAG", "onShown: ");
                            }
                        })
                        .setBackgroundColor(Color.LTGRAY)
                        .setActionTextColor(Color.BLUE)
                        .setTextColor(Color.RED)
                        .animate(FROM, TO)
                        .setGravity(GRAVITY)
                        .show();
            }
        });

        rgFrom.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rbFromTop:
                        FROM = AnimationBar.FROM_TOP;
                        break;
                    case R.id.rbFromBottom:
                        FROM = AnimationBar.FROM_BOTTOM;
                        break;
                    case R.id.rbFromRight:
                        FROM = AnimationBar.FROM_RIGHT;
                        break;
                    case R.id.rbFromLeft:
                        FROM = AnimationBar.FROM_LEFT;
                        break;
                }

            }
        });

        rgTo.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rbToTop:
                        TO = AnimationBar.TO_TOP;
                        break;
                    case R.id.rbToBottom:
                        TO = AnimationBar.TO_BOTTOM;
                        break;
                    case R.id.rbToRight:
                        TO = AnimationBar.TO_RIGHT;
                        break;
                    case R.id.rbToLeft:
                        TO = AnimationBar.TO_LEFT;
                        break;
                }

            }
        });

        rgGravity.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rbGravityBottom:
                        GRAVITY = Gravity.BOTTOM;
                        break;
                    case R.id.rbGravityTop:
                        GRAVITY = Gravity.TOP;
                        break;
                }
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
