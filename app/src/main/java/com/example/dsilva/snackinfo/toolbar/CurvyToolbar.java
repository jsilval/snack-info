package com.example.dsilva.snackinfo.toolbar;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewOutlineProvider;

public class CurvyToolbar extends Toolbar {

    private static float density = 1;
    private Paint mPaint; // para dibujar formas geométricas; redibujar la toolbar
    private Path mPath;
    private int viewWidth;
    private int viewHeight;
    private PointF mFirstCurveStartPoint;
    private PointF mFirstCurveEndPoint;
    private PointF mSecondCurveStartPoint;
    private PointF mSecondCurveEndPoint;
    private PointF mFirstCurveControlPoint1;
    private PointF mFirstCurveControlPoint2;
    private PointF mSecondCurveControlPoint1;
    private PointF mSecondCurveControlPoint2;
    private int CURVE_CIRCLE_RADIUS = 32;
    private CurvyToolbarOutLineProvider outLineProvider;

    public CurvyToolbar(Context context) {
        super(context);
        init(context);
    }

    public CurvyToolbar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CurvyToolbar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        mPath = new Path();
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL_AND_STROKE);
        mPaint.setColor(0xffCE510D);
        setBackgroundColor(Color.TRANSPARENT);

        mFirstCurveStartPoint = new PointF();
        mFirstCurveEndPoint = new PointF();
        mSecondCurveEndPoint = new PointF();
        mFirstCurveControlPoint1 = new PointF();
        mFirstCurveControlPoint2 = new PointF();
        mSecondCurveControlPoint1 = new PointF();
        mSecondCurveControlPoint2 = new PointF();

        density = context.getResources().getDisplayMetrics().density;
        CURVE_CIRCLE_RADIUS = dp(CURVE_CIRCLE_RADIUS);
        outLineProvider = new CurvyToolbarOutLineProvider();
//        setOutlineProvider(outLineProvider);
//        setClipToOutline(true);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(MeasureSpec.getSize(widthMeasureSpec), dp(56) + CURVE_CIRCLE_RADIUS);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        configurePath();
        Log.d("TAG", "onSizeChanged: ");
    }

    private void configurePath() {
        viewWidth = getWidth();
        viewHeight = getHeight();

        float x = (viewWidth / 2) + (CURVE_CIRCLE_RADIUS * 2) + (CURVE_CIRCLE_RADIUS / 3);
        float y = viewHeight - CURVE_CIRCLE_RADIUS;
        mFirstCurveStartPoint.set(x, y);

        x = (float) viewWidth / 2;
        y = viewHeight;
        mFirstCurveEndPoint.set(x, y);

        x = (viewWidth / 2) - (CURVE_CIRCLE_RADIUS * 2) - (CURVE_CIRCLE_RADIUS / 3);
        y = viewHeight - CURVE_CIRCLE_RADIUS;
        mSecondCurveStartPoint = mFirstCurveEndPoint;
        mSecondCurveEndPoint.set(x, y);

        mFirstCurveControlPoint1.set(mFirstCurveStartPoint.x - (CURVE_CIRCLE_RADIUS + (float) (CURVE_CIRCLE_RADIUS / 4)), mFirstCurveStartPoint.y);
        mFirstCurveControlPoint2.set(mFirstCurveEndPoint.x + CURVE_CIRCLE_RADIUS, mFirstCurveEndPoint.y);

        mSecondCurveControlPoint1.set(mSecondCurveStartPoint.x - CURVE_CIRCLE_RADIUS, mSecondCurveStartPoint.y);
        mSecondCurveControlPoint2.set(mSecondCurveEndPoint.x + (CURVE_CIRCLE_RADIUS + (float) (CURVE_CIRCLE_RADIUS / 4)), mSecondCurveEndPoint.y);

        mPath.reset();
        mPath.moveTo(0, 0); // P1

        mPath.lineTo(viewWidth, 0); // P2
        mPath.lineTo(viewWidth, viewHeight - CURVE_CIRCLE_RADIUS); // P3
        mPath.lineTo(mFirstCurveStartPoint.x, mFirstCurveStartPoint.y); // P4

        mPath.cubicTo(mFirstCurveControlPoint1.x, mFirstCurveControlPoint1.y,
                mFirstCurveControlPoint2.x, mFirstCurveControlPoint2.y,
                mFirstCurveEndPoint.x, mFirstCurveEndPoint.y);

        mPath.cubicTo(mSecondCurveControlPoint1.x, mSecondCurveControlPoint1.y,
                mSecondCurveControlPoint2.x, mSecondCurveControlPoint2.y,
                mSecondCurveEndPoint.x, mSecondCurveEndPoint.y);

        mPath.lineTo(0, viewHeight - CURVE_CIRCLE_RADIUS);
        mPath.close();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawPath(mPath, mPaint);
    }

    private static int dp(float value) {
        if (value == 0) {
            return 0;
        }
        return (int) Math.ceil(density * value);
    }

    private class CurvyToolbarOutLineProvider extends ViewOutlineProvider {

        @Override
        public void getOutline(View view, Outline outline) {
            configurePath();
//            if (mPath.isConvex()) {
                outline.setConvexPath(mPath);
//                Log.d("TAG", "getOutline: convex");
//            }
            Log.d("TAG", "getOutline: ");
        }

    }
}
