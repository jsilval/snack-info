package co.jsilval.snackinfo;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import androidx.annotation.IntRange;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.animation.AnimationUtils;

public class AnimationBar extends AnimatorListenerAdapter {
    public static final int FROM_LEFT = 1;
    public static final int FROM_RIGHT = 2;
    public static final int FROM_BOTTOM = 3;
    public static final int FROM_TOP = 4;

    public static final int TO_LEFT = 5;
    public static final int TO_RIGHT = 6;
    public static final int TO_BOTTOM = 7;
    public static final int TO_TOP = 8;

    private final int DELAY_IN = 70;
    private final int DELAY_OUT = 0;

    private static final int ANIMATION_DURATION = 250;
    private static final int ANIMATION_FADE_DURATION = 180;

    private View view;

    private int[] orientation = new int[2];

    private AnimationCallback animationCallback;

    public interface AnimationCallback {
        void onAnimationStart(int delay, int duration, boolean isInAnim);

        void onAnimationEnd(int event, boolean isInAnim);
    }

    AnimationBar(View view, AnimationCallback animationCallback) {
        this.view = view;
        this.animationCallback = animationCallback;
        orientation[0] = FROM_BOTTOM;
        orientation[1] = TO_BOTTOM;
    }

    void animateViewIn() {
        final int factor = getOrientationFactor(true);
        final int translation = getTranslation(true);

        applyTranslation(translation, factor, true);

        ValueAnimator animator = new ValueAnimator();
        animator.setIntValues(factor * translation, 0);

        animator.setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR);
        animator.setDuration(ANIMATION_DURATION);
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                animationCallback.onAnimationEnd(-1, true);
            }

            @Override
            public void onAnimationStart(Animator animation) {
                animationCallback.onAnimationStart(DELAY_IN, ANIMATION_FADE_DURATION, true);
            }
        });
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            public void onAnimationUpdate(ValueAnimator animator) {
                int currentAnimatedIntValue = (Integer) animator.getAnimatedValue();
                applyTranslation(currentAnimatedIntValue, factor, true);
            }
        });
        animator.start();
    }

    private int getOrientationFactor(boolean animIn) {
        int orientation = this.orientation[animIn ? 0 : 1];
        switch (orientation) {
            case FROM_LEFT:
            case FROM_BOTTOM:
            case TO_LEFT:
            case TO_TOP:
                return -1;
            default:
                return 1;
        }
    }

    void animateViewOut(final int event) {
        final int factor = getOrientationFactor(false);
        ValueAnimator animator = new ValueAnimator();
        animator.setIntValues(0, getTranslation(false));
        animator.setInterpolator(AnimationUtils.FAST_OUT_SLOW_IN_INTERPOLATOR);
        animator.setDuration(ANIMATION_DURATION);
        animator.addListener(new AnimatorListenerAdapter() {
            public void onAnimationStart(Animator animator) {
                animationCallback.onAnimationStart(DELAY_OUT, ANIMATION_FADE_DURATION, false);
            }

            public void onAnimationEnd(Animator animator) {
                animationCallback.onAnimationEnd(event, false);
            }
        });
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {

            public void onAnimationUpdate(ValueAnimator animator) {
                int currentAnimatedIntValue = (Integer) animator.getAnimatedValue();
                applyTranslation(currentAnimatedIntValue, factor, false);
            }
        });
        animator.start();
    }

    private void applyTranslation(int currentValue, int factor, boolean animIn) {
        int orientation = this.orientation[animIn ? 0 : 1];

        switch (orientation) {
            case FROM_BOTTOM:
                view.setTranslationY((float) currentValue);
            case FROM_TOP:
                view.setTranslationY((float) currentValue * -1);
                break;
            case FROM_LEFT:
                view.setTranslationX((float) currentValue);
            case FROM_RIGHT:
                view.setTranslationX((float) currentValue);
                break;
            case TO_BOTTOM:
                view.setTranslationY((float) currentValue);
                break;
            case TO_LEFT:
                view.setTranslationX((float) currentValue * factor);
                break;
            case TO_RIGHT:
                view.setTranslationX((float) currentValue);
                break;
            case TO_TOP:
                view.setTranslationY((float) currentValue * factor);
                break;

        }
    }

    private int getTranslation(boolean animIn) {
        int orientation = this.orientation[animIn ? 0 : 1];
        int translation;
        switch (orientation) {
            case FROM_BOTTOM:
            case FROM_TOP:
                translation = view.getHeight();
                break;
            case FROM_LEFT:
            case FROM_RIGHT:
                translation = view.getWidth();
                break;
            case TO_LEFT:
            case TO_RIGHT:
                translation = view.getWidth();
                break;
            case TO_BOTTOM:
            case TO_TOP:
                translation = view.getHeight();
                break;
            default:
                translation = view.getHeight();
        }

        ViewGroup.LayoutParams layoutParams = this.view.getLayoutParams();
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            translation += ((ViewGroup.MarginLayoutParams) layoutParams).rightMargin;
        }

        return translation;
    }


    void setOrientation(@IntRange(from = 1, to = 8) int fromIn, @IntRange(from = 1, to = 8) int toOut) {
        orientation[0] = fromIn;
        orientation[1] = toOut;
    }
}

