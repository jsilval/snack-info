/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package co.jsilval.snackinfo;

import android.accessibilityservice.AccessibilityServiceInfo;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityManager;
import android.widget.FrameLayout;

import androidx.annotation.ColorInt;
import androidx.annotation.IntRange;
import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.RestrictTo;
import androidx.annotation.RestrictTo.Scope;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.view.AccessibilityDelegateCompat;
import androidx.core.view.OnApplyWindowInsetsListener;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;
import androidx.core.view.accessibility.AccessibilityManagerCompat;
import androidx.core.view.accessibility.AccessibilityManagerCompat.TouchExplorationStateChangeListener;
import androidx.core.view.accessibility.AccessibilityNodeInfoCompat;

import com.google.android.material.R.attr;
import com.google.android.material.R.styleable;
import com.google.android.material.behavior.SwipeDismissBehavior;
import com.google.android.material.behavior.SwipeDismissBehavior.OnDismissListener;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

public abstract class BaseTransientBottomBar<B extends BaseTransientBottomBar<B>>
        implements AnimationBar.AnimationCallback {

    public static final int LENGTH_INDEFINITE = -2;
    public static final int LENGTH_SHORT = -1;
    public static final int LENGTH_LONG = 0;

    private static final int[] SNACKBAR_STYLE_ATTR;
    private static final int[] APPCOMPAT_CHECK_ATTRS;

    static final Handler handler;
    static final int MSG_SHOW = 0;
    static final int MSG_DISMISS = 1;
    private final ViewGroup targetParent;
    private final Context context;
    protected final BaseTransientBottomBar.SnackInfoBaseLayout view;
    private final ContentViewCallback contentViewCallback;
    private int duration;
    private List<BaseCallback<B>> callbacks;
    private BaseTransientBottomBar.Behavior behavior;
    private final AccessibilityManager accessibilityManager;
    private int currentGravity;

    private AnimationBar animationBar;

    private final SnackInfoManager.Callback managerCallback = new SnackInfoManager.Callback() {
        public void show() {
            BaseTransientBottomBar.handler.sendMessage(
                    handler.obtainMessage(MSG_SHOW, BaseTransientBottomBar.this));
        }

        public void dismiss(int event) {
            handler.sendMessage(handler.obtainMessage(MSG_DISMISS, event, 0, BaseTransientBottomBar.this));
        }
    };

    static {
        APPCOMPAT_CHECK_ATTRS = new int[]{attr.colorPrimary};
    }


    private static void checkTheme(Context context, int[] themeAttributes, String themeName) {
        if (!isTheme(context, themeAttributes)) {
            throw new IllegalArgumentException("The style on this component requires your app theme to be " + themeName + " (or a descendant).");
        }
    }

    private static boolean isTheme(Context context, int[] themeAttributes) {
        TypedArray a = context.obtainStyledAttributes(themeAttributes);
        boolean success = a.hasValue(0);
        a.recycle();
        return success;
    }

    private static void checkAppCompatTheme(Context context) {
        checkTheme(context, APPCOMPAT_CHECK_ATTRS, "Theme.AppCompat");
    }

    protected BaseTransientBottomBar(ViewGroup parent, View content,
                                     ContentViewCallback contentViewCallback) {
        if (parent == null) {
            throw new IllegalArgumentException("Transient bottom bar must have non-null parent");
        } else if (content == null) {
            throw new IllegalArgumentException("Transient bottom bar must have non-null content");
        } else if (contentViewCallback == null) {
            throw new IllegalArgumentException("Transient bottom bar must have non-null callback");
        } else {
            this.targetParent = parent;
            this.contentViewCallback = contentViewCallback;
            this.context = parent.getContext();
            checkAppCompatTheme(this.context);
            LayoutInflater inflater = LayoutInflater.from(this.context);
            this.view = (BaseTransientBottomBar.SnackInfoBaseLayout) inflater.inflate(this.getSnackbarBaseLayoutResId(), this.targetParent, false);
            this.view.addView(content);
            this.view.getChildAt(0).setBackground(Theme.createRoundRectDrawable(context, 8, Color.BLACK));

            ViewCompat.setAccessibilityLiveRegion(this.view, ViewCompat.ACCESSIBILITY_LIVE_REGION_POLITE);
            ViewCompat.setImportantForAccessibility(this.view, ViewCompat.IMPORTANT_FOR_ACCESSIBILITY_YES);
            ViewCompat.setFitsSystemWindows(this.view, true);
            ViewCompat.setOnApplyWindowInsetsListener(this.view, new OnApplyWindowInsetsListener() {

                @Override
                public WindowInsetsCompat onApplyWindowInsets(View v, WindowInsetsCompat insets) {
                    v.setPadding(v.getPaddingLeft(), v.getPaddingTop(), v.getPaddingRight(), insets.getSystemWindowInsetBottom());
                    return insets;
                }
            });
            ViewCompat.setAccessibilityDelegate(this.view, new AccessibilityDelegateCompat() {
                @Override
                public void onInitializeAccessibilityNodeInfo(View host, AccessibilityNodeInfoCompat info) {
                    super.onInitializeAccessibilityNodeInfo(host, info);
                    info.addAction(AccessibilityNodeInfoCompat.ACTION_DISMISS);
                    info.setDismissable(true);
                }

                @Override
                public boolean performAccessibilityAction(View host, int action, Bundle args) {
                    if (action == AccessibilityNodeInfoCompat.ACTION_DISMISS) {
                        dismiss();
                        return true;
                    } else {
                        return super.performAccessibilityAction(host, action, args);
                    }
                }
            });
            this.accessibilityManager = (AccessibilityManager) this.context.getSystemService(Context.ACCESSIBILITY_SERVICE);

            animationBar = new AnimationBar(view, this);

        }
    }

    @LayoutRes
    protected int getSnackbarBaseLayoutResId() {
        return this.hasSnackbarStyleAttr() ? R.layout.snackinfo_mtrl_layout : R.layout.snackinfo_design_layout;
    }

    protected boolean hasSnackbarStyleAttr() {
        TypedArray a = this.context.obtainStyledAttributes(SNACKBAR_STYLE_ATTR);
        int snackInfoStyleResId = a.getResourceId(0, -1);
        a.recycle();
        return snackInfoStyleResId != -1;
    }

    @NonNull
    public B setDuration(int duration) {
        this.duration = duration;
        return (B) this;
    }

    public int getDuration() {
        return this.duration;
    }

    public B setBehavior(BaseTransientBottomBar.Behavior behavior) {
        this.behavior = behavior;
        return (B) this;
    }

    public BaseTransientBottomBar.Behavior getBehavior() {
        return this.behavior;
    }

    @NonNull
    public Context getContext() {
        return this.context;
    }

    @NonNull
    public View getView() {
        return this.view;
    }

    public void show() {
        SnackInfoManager.getInstance().show(this.getDuration(), this.managerCallback);
    }

    public void dismiss() {
        this.dispatchDismiss(BaseCallback.DISMISS_EVENT_SWIPE);
    }

    protected void dispatchDismiss(int event) {
        SnackInfoManager.getInstance().dismiss(this.managerCallback, event);
    }

    @NonNull
    public B addCallback(@NonNull BaseTransientBottomBar.BaseCallback<B> callback) {
        if (callback == null) {
            return (B) this;
        } else {
            if (this.callbacks == null) {
                this.callbacks = new ArrayList();
            }

            this.callbacks.add(callback);
            return (B) this;
        }
    }

    @NonNull
    public B removeCallback(@NonNull BaseTransientBottomBar.BaseCallback<B> callback) {
        if (callback == null) {
            return (B) this;
        } else if (this.callbacks == null) {
            return (B) this;
        } else {
            this.callbacks.remove(callback);
            return (B) this;
        }
    }


    @NonNull
    public B animate(@IntRange(from = 1, to = 4) int fromIn, @IntRange(from = 5, to = 8) int toOut) {
        animationBar.setOrientation(fromIn, toOut);
        return (B) this;
    }

    @NonNull
    public B setGravity(int gravity) {
        currentGravity = gravity;
        final LayoutParams lp = this.view.getLayoutParams();
        if (lp instanceof CoordinatorLayout.LayoutParams) {
            CoordinatorLayout.LayoutParams clp = (CoordinatorLayout.LayoutParams) lp;
            clp.gravity = gravity;
            view.setLayoutParams(clp);
        } else {
            FrameLayout.LayoutParams flp = (FrameLayout.LayoutParams) lp;
            flp.gravity = gravity;
            view.setLayoutParams(flp);
        }
        return (B) this;
    }

    public B setBackgroundColor(@ColorInt int color) {
        this.view.getChildAt(0).setBackground(Theme.createRoundRectDrawable(context, 8, color));
        return (B) this;
    }

    public boolean isShown() {
        return SnackInfoManager.getInstance().isCurrent(this.managerCallback);
    }

    public boolean isShownOrQueued() {
        return SnackInfoManager.getInstance().isCurrentOrNext(this.managerCallback);
    }

    protected SwipeDismissBehavior<? extends View> getNewBehavior() {
        return new BaseTransientBottomBar.Behavior();
    }

    final void showView() {
        if (this.view.getParent() == null) {
            LayoutParams lp = this.view.getLayoutParams();
            if (lp instanceof CoordinatorLayout.LayoutParams) {
                CoordinatorLayout.LayoutParams clp = (CoordinatorLayout.LayoutParams) lp;
                SwipeDismissBehavior<? extends View> behavior = this.behavior == null ? this.getNewBehavior() : this.behavior;
                if (behavior instanceof BaseTransientBottomBar.Behavior) {
                    ((BaseTransientBottomBar.Behavior) behavior).setBaseTransientBottomBar(this);
                }

                ((SwipeDismissBehavior) behavior).setListener(new OnDismissListener() {
                    public void onDismiss(View view) {
                        view.setVisibility(View.GONE);
                        BaseTransientBottomBar.this.dispatchDismiss(BaseCallback.DISMISS_EVENT_SWIPE);
                    }

                    public void onDragStateChanged(int state) {
                        switch (state) {
                            case SwipeDismissBehavior.STATE_IDLE:
                                SnackInfoManager.getInstance().restoreTimeoutIfPaused(BaseTransientBottomBar.this.managerCallback);
                                break;
                            case SwipeDismissBehavior.STATE_DRAGGING:
                            case SwipeDismissBehavior.STATE_SETTLING:
                                SnackInfoManager.getInstance().pauseTimeout(BaseTransientBottomBar.this.managerCallback);
                                break;
                            default:
                        }

                    }
                });

                if (currentGravity == Gravity.BOTTOM) {
                    clp.setBehavior((CoordinatorLayout.Behavior) behavior);
                    clp.insetEdge = 80;
                }
            }

            this.targetParent.addView(this.view);
        }

        this.view.setOnAttachStateChangeListener(new BaseTransientBottomBar.OnAttachStateChangeListener() {
            public void onViewAttachedToWindow(View v) {
            }

            public void onViewDetachedFromWindow(View v) {
                if (BaseTransientBottomBar.this.isShownOrQueued()) {
                    BaseTransientBottomBar.handler.post(new Runnable() {
                        public void run() {
                            BaseTransientBottomBar.this.onViewHidden(BaseCallback.DISMISS_EVENT_MANUAL);
                        }
                    });
                }

            }
        });
        if (ViewCompat.isLaidOut(this.view)) {
            if (this.shouldAnimate()) {
                animationBar.animateViewIn();
            } else {
                this.onViewShown();
            }
        } else {
            this.view.setOnLayoutChangeListener(new BaseTransientBottomBar.OnLayoutChangeListener() {
                public void onLayoutChange(View view, int left, int top, int right, int bottom) {
                    BaseTransientBottomBar.this.view.setOnLayoutChangeListener(null);
                    if (BaseTransientBottomBar.this.shouldAnimate()) {
                        animationBar.animateViewIn();
                    } else {
                        BaseTransientBottomBar.this.onViewShown();
                    }

                }
            });
        }

    }

    private void hideView(int event) {
        if (this.shouldAnimate() && this.view.getVisibility() == View.VISIBLE) {
            animationBar.animateViewOut(event);
        } else {
            this.onViewHidden(event);
        }

    }

    private void onViewShown() {
        SnackInfoManager.getInstance().onShown(this.managerCallback);
        if (this.callbacks != null) {
            int callbackCount = this.callbacks.size();

            for (int i = callbackCount - 1; i >= 0; --i) {
                callbacks.get(i).onShown((B) this);
            }
        }

    }

    private void onViewHidden(int event) {
        SnackInfoManager.getInstance().onDismissed(this.managerCallback);
        if (this.callbacks != null) {
            int callbackCount = this.callbacks.size();

            for (int i = callbackCount - 1; i >= 0; --i) {
                callbacks.get(i).onDismissed((B) this, event);
            }
        }

        ViewParent parent = this.view.getParent();
        if (parent instanceof ViewGroup) {
            ((ViewGroup) parent).removeView(this.view);
        }

    }

    private boolean shouldAnimate() {
        final int feedbackFlags = AccessibilityServiceInfo.FEEDBACK_SPOKEN;
        List<AccessibilityServiceInfo> serviceList =
                this.accessibilityManager.getEnabledAccessibilityServiceList(feedbackFlags);
        return serviceList != null && serviceList.isEmpty();
    }

    static {
        SNACKBAR_STYLE_ATTR = new int[]{R.attr.snackbarStyle};
        handler = new Handler(Looper.getMainLooper(), new Handler.Callback() {
            @Override
            public boolean handleMessage(Message message) {
                switch (message.what) {
                    case MSG_SHOW:
                        ((BaseTransientBottomBar) message.obj).showView();
                        return true;
                    case MSG_DISMISS:
                        ((BaseTransientBottomBar) message.obj).hideView(message.arg1);
                        return true;
                    default:
                        return false;
                }
            }
        });
    }

    @Override
    public void onAnimationStart(int delay, int duration, boolean isInAnim) {
        if (isInAnim) {
            contentViewCallback.animateContentIn(delay, duration);
        } else {
            contentViewCallback.animateContentOut(delay, duration);
        }
    }

    @Override
    public void onAnimationEnd(int event, boolean isInAnim) {
        if (isInAnim) {
            onViewShown();
        } else {
            onViewHidden(event);
        }
    }

    @RestrictTo({Scope.LIBRARY_GROUP})
    public static class BehaviorDelegate {
        private SnackInfoManager.Callback managerCallback;

        BehaviorDelegate(SwipeDismissBehavior<?> behavior) {
            behavior.setStartAlphaSwipeDistance(0.1F);
            behavior.setEndAlphaSwipeDistance(0.6F);
            behavior.setSwipeDirection(SwipeDismissBehavior.SWIPE_DIRECTION_START_TO_END);
        }

        void setBaseTransientBottomBar(BaseTransientBottomBar<?> baseTransientBottomBar) {
            this.managerCallback = baseTransientBottomBar.managerCallback;
        }

        boolean canSwipeDismissView(View child) {
            return child instanceof BaseTransientBottomBar.SnackInfoBaseLayout;
        }

        void onInterceptTouchEvent(CoordinatorLayout parent, View child, MotionEvent event) {
            switch (event.getActionMasked()) {
                case MotionEvent.ACTION_DOWN:
                    if (parent.isPointInChildBounds(child, (int) event.getX(), (int) event.getY())) {
                        SnackInfoManager.getInstance().pauseTimeout(this.managerCallback);
                    }
                    break;
                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:
                    SnackInfoManager.getInstance().restoreTimeoutIfPaused(this.managerCallback);
                    break;
                default:
                    break;
            }

        }
    }

    public static class Behavior extends SwipeDismissBehavior<View> {
        private final BaseTransientBottomBar.BehaviorDelegate delegate = new BaseTransientBottomBar.BehaviorDelegate(this);

        Behavior() {
        }

        private void setBaseTransientBottomBar(BaseTransientBottomBar<?> baseTransientBottomBar) {
            this.delegate.setBaseTransientBottomBar(baseTransientBottomBar);
        }

        @Override
        public boolean canSwipeDismissView(@NonNull View child) {
            return this.delegate.canSwipeDismissView(child);
        }

        @Override
        public boolean onInterceptTouchEvent(CoordinatorLayout parent, View child, MotionEvent event) {
            this.delegate.onInterceptTouchEvent(parent, child, event);
            return super.onInterceptTouchEvent(parent, child, event);
        }
    }

    @RestrictTo({Scope.LIBRARY_GROUP})
    protected static class SnackInfoBaseLayout extends FrameLayout {
        private final AccessibilityManager accessibilityManager;
        private final TouchExplorationStateChangeListener touchExplorationStateChangeListener;
        private BaseTransientBottomBar.OnLayoutChangeListener onLayoutChangeListener;
        private BaseTransientBottomBar.OnAttachStateChangeListener onAttachStateChangeListener;

        protected SnackInfoBaseLayout(Context context) {
            this(context, (AttributeSet) null);
        }

        protected SnackInfoBaseLayout(Context context, AttributeSet attrs) {
            super(context, attrs);
            TypedArray a = context.obtainStyledAttributes(attrs, styleable.SnackbarLayout);
            if (a.hasValue(styleable.SnackbarLayout_elevation)) {
                ViewCompat.setElevation(this, (float) a.getDimensionPixelSize(styleable.SnackbarLayout_elevation, 0));
            }

            a.recycle();
            this.accessibilityManager = (AccessibilityManager) context.getSystemService(Context.ACCESSIBILITY_SERVICE);
            this.touchExplorationStateChangeListener = new TouchExplorationStateChangeListener() {
                public void onTouchExplorationStateChanged(boolean enabled) {
                    BaseTransientBottomBar.SnackInfoBaseLayout.this.setClickableOrFocusableBasedOnAccessibility(enabled);
                }
            };
            AccessibilityManagerCompat.addTouchExplorationStateChangeListener(this.accessibilityManager, this.touchExplorationStateChangeListener);
            this.setClickableOrFocusableBasedOnAccessibility(this.accessibilityManager.isTouchExplorationEnabled());
        }

        private void setClickableOrFocusableBasedOnAccessibility(boolean touchExplorationEnabled) {
            this.setClickable(!touchExplorationEnabled);
            this.setFocusable(touchExplorationEnabled);
        }

        protected void onLayout(boolean changed, int l, int t, int r, int b) {
            super.onLayout(changed, l, t, r, b);
            if (this.onLayoutChangeListener != null) {
                this.onLayoutChangeListener.onLayoutChange(this, l, t, r, b);
            }

        }

        protected void onAttachedToWindow() {
            super.onAttachedToWindow();
            if (this.onAttachStateChangeListener != null) {
                this.onAttachStateChangeListener.onViewAttachedToWindow(this);
            }

            ViewCompat.requestApplyInsets(this);
        }

        protected void onDetachedFromWindow() {
            super.onDetachedFromWindow();
            if (this.onAttachStateChangeListener != null) {
                this.onAttachStateChangeListener.onViewDetachedFromWindow(this);
            }

            AccessibilityManagerCompat.removeTouchExplorationStateChangeListener(this.accessibilityManager, this.touchExplorationStateChangeListener);
        }

        void setOnLayoutChangeListener(BaseTransientBottomBar.OnLayoutChangeListener onLayoutChangeListener) {
            this.onLayoutChangeListener = onLayoutChangeListener;
        }

        void setOnAttachStateChangeListener(BaseTransientBottomBar.OnAttachStateChangeListener listener) {
            this.onAttachStateChangeListener = listener;
        }
    }

    @RestrictTo({Scope.LIBRARY_GROUP})
    protected interface OnAttachStateChangeListener {
        void onViewAttachedToWindow(View view);

        void onViewDetachedFromWindow(View view);
    }

    @RestrictTo({Scope.LIBRARY_GROUP})
    protected interface OnLayoutChangeListener {
        void onLayoutChange(View view, int left, int top, int right, int bottom);
    }

    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({Scope.LIBRARY_GROUP})
    @IntRange(
            from = 1L
    )
    public @interface Duration {
    }

    public abstract static class BaseCallback<B> {
        public static final int DISMISS_EVENT_SWIPE = 0;
        public static final int DISMISS_EVENT_ACTION = 1;
        public static final int DISMISS_EVENT_TIMEOUT = 2;
        public static final int DISMISS_EVENT_MANUAL = 3;
        public static final int DISMISS_EVENT_CONSECUTIVE = 4;

        public BaseCallback() {
        }

        public void onDismissed(B transientBottomBar, int event) {
        }

        public void onShown(B transientBottomBar) {
        }

        @Retention(RetentionPolicy.SOURCE)
        @RestrictTo({Scope.LIBRARY_GROUP})
        public @interface DismissEvent {
        }
    }
}

