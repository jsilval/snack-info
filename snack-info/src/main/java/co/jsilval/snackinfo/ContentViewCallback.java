package co.jsilval.snackinfo;

public interface ContentViewCallback {
    void animateContentIn(int delay, int duration);

    void animateContentOut(int delay, int duration);
}
