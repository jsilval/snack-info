/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package co.jsilval.snackinfo;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.accessibility.AccessibilityManager;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RestrictTo;
import androidx.annotation.StringRes;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.google.android.material.R.attr;
import com.google.android.material.snackbar.Snackbar;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class SnackInfo extends BaseTransientBottomBar<SnackInfo> {
    private final AccessibilityManager accessibilityManager;
    private boolean hasAction;

    public static final int LENGTH_INDEFINITE = -2;
    public static final int LENGTH_SHORT = -1;
    public static final int LENGTH_LONG = 0;

    private static final int[] SNACKBAR_BUTTON_STYLE_ATTR;

    @Nullable
    private BaseCallback<SnackInfo> callback;

    private SnackInfo(ViewGroup parent, View content, ContentViewCallback contentViewCallback) {
        super(parent, content, contentViewCallback);
        this.accessibilityManager = (AccessibilityManager) parent.getContext().getSystemService(Context.ACCESSIBILITY_SERVICE);
    }

    public void show() {
        super.show();
    }

    public void dismiss() {
        super.dismiss();
    }

    public boolean isShown() {
        return super.isShown();
    }

    @NonNull
    public static SnackInfo make(@NonNull View view, @NonNull CharSequence text, int duration) {
        ViewGroup parent = findSuitableParent(view);
        if (parent == null) {
            throw new IllegalArgumentException("No suitable parent found from the given view. Please provide a valid view.");
        } else {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            SnackInfoContentLayout content = (SnackInfoContentLayout) inflater
                    .inflate(hasSnackbarButtonStyleAttr(parent.getContext()) ?
                            R.layout.snackinfo_mtrl_layout_include :
                            R.layout.snackinfo_design_layout_include, parent, false);
            SnackInfo snackbar = new SnackInfo(parent, content, content);
            snackbar.setText(text);
            snackbar.setDuration(duration);
            return snackbar;
        }
    }

    protected static boolean hasSnackbarButtonStyleAttr(Context context) {
        TypedArray a = context.obtainStyledAttributes(SNACKBAR_BUTTON_STYLE_ATTR);
        int snackbarButtonStyleResId = a.getResourceId(0, -1);
        a.recycle();
        return snackbarButtonStyleResId != -1;
    }

    @NonNull
    public static SnackInfo make(@NonNull View view, @StringRes int resId, int duration) {
        return make(view, view.getResources().getText(resId), duration);
    }

    private static ViewGroup findSuitableParent(View view) {
        ViewGroup fallback = null;

        do {
            if (view instanceof CoordinatorLayout) {
                return (ViewGroup) view;
            }

            if (view instanceof FrameLayout) {
                if (view.getId() == android.R.id.content) {
                    return (ViewGroup) view;
                }

                fallback = (ViewGroup) view;
            }

            if (view != null) {
                ViewParent parent = view.getParent();
                view = parent instanceof View ? (View) parent : null;
            }
        } while (view != null);

        return fallback;
    }

    @NonNull
    public SnackInfo setText(@NonNull CharSequence message) {
        SnackInfoContentLayout contentLayout = (SnackInfoContentLayout) this.view.getChildAt(0);
        TextView tv = contentLayout.getMessageView();
        tv.setText(message);
        return this;
    }

    @NonNull
    public SnackInfo setText(@StringRes int resId) {
        return this.setText(this.getContext().getText(resId));
    }

    @NonNull
    public SnackInfo setAction(@StringRes int resId, View.OnClickListener listener) {
        return this.setAction(this.getContext().getText(resId), listener);
    }

    @NonNull
    public SnackInfo setAction(CharSequence text, final View.OnClickListener listener) {
        SnackInfoContentLayout contentLayout = (SnackInfoContentLayout) this.view.getChildAt(0);
        TextView tv = contentLayout.getActionView();
        if (!TextUtils.isEmpty(text) && listener != null) {
            this.hasAction = true;
            tv.setVisibility(View.VISIBLE);
            tv.setText(text);
            tv.setOnClickListener(new View.OnClickListener() {
                public void onClick(View view) {
                    listener.onClick(view);
                    SnackInfo.this.dispatchDismiss(BaseTransientBottomBar.BaseCallback.DISMISS_EVENT_ACTION);
                }
            });
        } else {
            tv.setVisibility(View.GONE);
            tv.setOnClickListener(null);
            this.hasAction = false;
        }

        return this;
    }

    public int getDuration() {
        return this.hasAction && this.accessibilityManager.isTouchExplorationEnabled() ? LENGTH_INDEFINITE : super.getDuration();
    }

    @NonNull
    public SnackInfo setActionTextColor(ColorStateList colors) {
        SnackInfoContentLayout contentLayout = (SnackInfoContentLayout) this.view.getChildAt(0);
        TextView tv = contentLayout.getActionView();
        tv.setTextColor(colors);
        return this;
    }

    @NonNull
    public SnackInfo setActionTextColor(@ColorInt int color) {
        SnackInfoContentLayout contentLayout = (SnackInfoContentLayout) this.view.getChildAt(0);
        TextView tv = contentLayout.getActionView();
        tv.setTextColor(color);
        return this;
    }

    @NonNull
    public SnackInfo setTextColor(@ColorInt int color) {
        SnackInfoContentLayout contentLayout = (SnackInfoContentLayout) this.view.getChildAt(0);
        TextView tv = contentLayout.getMessageView();
        tv.setTextColor(color);
        return this;
    }

    @NonNull
    public SnackInfo setTextColor(ColorStateList colors) {
        SnackInfoContentLayout contentLayout = (SnackInfoContentLayout) this.view.getChildAt(0);
        TextView tv = contentLayout.getMessageView();
        tv.setTextColor(colors);
        return this;
    }

    /**
     * @deprecated
     */
    @Deprecated
    @NonNull
    public SnackInfo setCallback(SnackInfo.Callback callback) {
        if (this.callback != null) {
            this.removeCallback(this.callback);
        }

        if (callback != null) {
            this.addCallback(callback);
        }

        this.callback = callback;
        return this;
    }

    static {
        SNACKBAR_BUTTON_STYLE_ATTR = new int[]{attr.snackbarButtonStyle};
    }

    public static final class SnackInfoLayout extends SnackInfoBaseLayout {
        public SnackInfoLayout(Context context) {
            super(context);
        }

        public SnackInfoLayout(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            int childCount = this.getChildCount();
            int availableWidth = this.getMeasuredWidth() - this.getPaddingLeft() - this.getPaddingRight();

            for(int i = 0; i < childCount; ++i) {
                View child = this.getChildAt(i);
                if (child.getLayoutParams().width == -1) {
                    child.measure(MeasureSpec.makeMeasureSpec(availableWidth, MeasureSpec.EXACTLY),
                            MeasureSpec.makeMeasureSpec(child.getMeasuredHeight(), MeasureSpec.EXACTLY));
                }
            }

        }
    }

    public static class Callback extends BaseCallback<SnackInfo> {
        public static final int DISMISS_EVENT_SWIPE = 0;
        public static final int DISMISS_EVENT_ACTION = 1;
        public static final int DISMISS_EVENT_TIMEOUT = 2;
        public static final int DISMISS_EVENT_MANUAL = 3;
        public static final int DISMISS_EVENT_CONSECUTIVE = 4;

        public Callback() {
        }

        public void onShown(Snackbar sb) {
        }

        public void onDismissed(Snackbar transientBottomBar, int event) {
        }
    }

    @Retention(RetentionPolicy.SOURCE)
    @RestrictTo({RestrictTo.Scope.LIBRARY_GROUP})
    @IntRange(
            from = 1L
    )
    public @interface Duration {
    }
}
