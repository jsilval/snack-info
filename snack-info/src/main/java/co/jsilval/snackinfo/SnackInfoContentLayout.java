/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package co.jsilval.snackinfo;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.view.ViewCompat;

public class SnackInfoContentLayout extends LinearLayout implements ContentViewCallback {
    private TextView messageView;
    private Button actionView;
    private int maxWidth;
    private int maxInlineActionWidth;

    public SnackInfoContentLayout(Context context) {
        this(context, null);
    }

    public SnackInfoContentLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, co.jsilval.snackinfo.R.styleable.SnackbarLayout);
        this.maxWidth = a.getDimensionPixelSize(co.jsilval.snackinfo.R.styleable.SnackbarLayout_maxWidth, -1);
        this.maxInlineActionWidth = a.getDimensionPixelSize(co.jsilval.snackinfo.R.styleable.SnackbarLayout_maxActionInlineWidth_, -1);
        a.recycle();
    }

    protected void onFinishInflate() {
        super.onFinishInflate();
        this.messageView = (TextView) this.findViewById(R.id.snackbar_text);
        this.actionView = (Button) this.findViewById(R.id.snackbar_action);
    }

    public TextView getMessageView() {
        return this.messageView;
    }

    public Button getActionView() {
        return this.actionView;
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (this.maxWidth > 0 && this.getMeasuredWidth() > this.maxWidth) {
            widthMeasureSpec = MeasureSpec.makeMeasureSpec(this.maxWidth, MeasureSpec.EXACTLY);
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }

        int multiLineVPadding = this.getResources().getDimensionPixelSize(co.jsilval.snackinfo.R.dimen.design_snackbar_padding_vertical_2lines_);
        int singleLineVPadding = this.getResources().getDimensionPixelSize(co.jsilval.snackinfo.R.dimen.design_snackbar_padding_vertical_);
        boolean isMultiLine = this.messageView.getLayout().getLineCount() > 1;
        boolean remeasure = false;
        if (isMultiLine
                && this.maxInlineActionWidth > 0
                && this.actionView.getMeasuredWidth() > this.maxInlineActionWidth) {
            if (this.updateViewsWithinLayout(VERTICAL, multiLineVPadding, multiLineVPadding - singleLineVPadding)) {
                remeasure = true;
            }
        } else {
            int messagePadding = isMultiLine ? multiLineVPadding : singleLineVPadding;
            if (this.updateViewsWithinLayout(HORIZONTAL, messagePadding, messagePadding)) {
                remeasure = true;
            }
        }

        if (remeasure) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }

    }

    private boolean updateViewsWithinLayout(int orientation, int messagePadTop, int messagePadBottom) {
        boolean changed = false;
        if (orientation != this.getOrientation()) {
            this.setOrientation(orientation);
            changed = true;
        }

        if (this.messageView.getPaddingTop() != messagePadTop || this.messageView.getPaddingBottom() != messagePadBottom) {
            updateTopBottomPadding(this.messageView, messagePadTop, messagePadBottom);
            changed = true;
        }

        return changed;
    }

    private static void updateTopBottomPadding(View view, int topPadding, int bottomPadding) {
        if (ViewCompat.isPaddingRelative(view)) {
            ViewCompat.setPaddingRelative(view, ViewCompat.getPaddingStart(view), topPadding, ViewCompat.getPaddingEnd(view), bottomPadding);
        } else {
            view.setPadding(view.getPaddingLeft(), topPadding, view.getPaddingRight(), bottomPadding);
        }

    }

    public void animateContentIn(int delay, int duration) {
        this.messageView.setAlpha(0.0F);
        this.messageView.animate().alpha(1.0F).setDuration(duration).setStartDelay(delay).start();
        if (this.actionView.getVisibility() == View.VISIBLE) {
            this.actionView.setAlpha(0.0F);
            this.actionView.animate().alpha(1.0F).setDuration(duration).setStartDelay(delay).start();
        }
    }

    public void animateContentOut(int delay, int duration) {
        this.messageView.setAlpha(1.0F);
        this.messageView.animate().alpha(0.0F).setDuration(duration).setStartDelay(delay).start();
        if (this.actionView.getVisibility() == View.VISIBLE) {
            this.actionView.setAlpha(1.0F);
            this.actionView.animate().alpha(0.0F).setDuration(duration).setStartDelay(delay).start();
        }

    }
}
