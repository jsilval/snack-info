/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package co.jsilval.snackinfo;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import java.lang.ref.WeakReference;

class SnackInfoManager {
    static final int MSG_TIMEOUT = 0;
    private static final int SHORT_DURATION_MS = 1500;
    private static final int LONG_DURATION_MS = 2750;
    private static SnackInfoManager snackInfoManager;
    private final Object lock = new Object();

    private final Handler handler = new Handler(Looper.getMainLooper(), new Handler.Callback() {
        public boolean handleMessage(Message message) {
            switch (message.what) {
                case MSG_TIMEOUT:
                    SnackInfoManager.this.handleTimeout((SnackInfoManager.SnackbarRecord) message.obj);
                    return true;
                default:
                    return false;
            }
        }
    });
    private SnackInfoManager.SnackbarRecord currentSnackbar;
    private SnackInfoManager.SnackbarRecord nextSnackbar;

    static SnackInfoManager getInstance() {
        if (snackInfoManager == null) {
            snackInfoManager = new SnackInfoManager();
        }

        return snackInfoManager;
    }

    private SnackInfoManager() {
    }

    public void show(int duration, SnackInfoManager.Callback callback) {
        synchronized (lock) {
            if (isCurrentSnackbarLocked(callback)) {
                currentSnackbar.duration = duration;
                handler.removeCallbacksAndMessages(this.currentSnackbar);
                scheduleTimeoutLocked(this.currentSnackbar);
            } else {
                if (this.isNextSnackbarLocked(callback)) {
                    nextSnackbar.duration = duration;
                } else {
                    nextSnackbar = new SnackInfoManager.SnackbarRecord(duration, callback);
                }

                if (currentSnackbar == null || !this.cancelSnackbarLocked(this.currentSnackbar, 4)) {
                    currentSnackbar = null;
                    this.showNextSnackbarLocked();
                }
            }
        }
    }

    public void dismiss(SnackInfoManager.Callback callback, int event) {
        Object var3 = this.lock;
        synchronized (this.lock) {
            if (this.isCurrentSnackbarLocked(callback)) {
                this.cancelSnackbarLocked(this.currentSnackbar, event);
            } else if (this.isNextSnackbarLocked(callback)) {
                this.cancelSnackbarLocked(this.nextSnackbar, event);
            }

        }
    }

    public void onDismissed(SnackInfoManager.Callback callback) {
        Object var2 = this.lock;
        synchronized (this.lock) {
            if (this.isCurrentSnackbarLocked(callback)) {
                this.currentSnackbar = null;
                if (this.nextSnackbar != null) {
                    this.showNextSnackbarLocked();
                }
            }

        }
    }

    public void onShown(SnackInfoManager.Callback callback) {
        Object var2 = this.lock;
        synchronized (this.lock) {
            if (this.isCurrentSnackbarLocked(callback)) {
                this.scheduleTimeoutLocked(this.currentSnackbar);
            }

        }
    }

    public void pauseTimeout(SnackInfoManager.Callback callback) {
        Object var2 = this.lock;
        synchronized (this.lock) {
            if (this.isCurrentSnackbarLocked(callback) && !this.currentSnackbar.paused) {
                this.currentSnackbar.paused = true;
                this.handler.removeCallbacksAndMessages(this.currentSnackbar);
            }

        }
    }

    public void restoreTimeoutIfPaused(SnackInfoManager.Callback callback) {
        Object var2 = this.lock;
        synchronized (this.lock) {
            if (this.isCurrentSnackbarLocked(callback) && this.currentSnackbar.paused) {
                this.currentSnackbar.paused = false;
                this.scheduleTimeoutLocked(this.currentSnackbar);
            }

        }
    }

    public boolean isCurrent(SnackInfoManager.Callback callback) {
        Object var2 = this.lock;
        synchronized (this.lock) {
            return this.isCurrentSnackbarLocked(callback);
        }
    }

    public boolean isCurrentOrNext(SnackInfoManager.Callback callback) {
        Object var2 = this.lock;
        synchronized (this.lock) {
            return this.isCurrentSnackbarLocked(callback) || this.isNextSnackbarLocked(callback);
        }
    }

    private void showNextSnackbarLocked() {
        if (this.nextSnackbar != null) {
            this.currentSnackbar = this.nextSnackbar;
            this.nextSnackbar = null;
            SnackInfoManager.Callback callback = this.currentSnackbar.callback.get();
            if (callback != null) {
                callback.show();
            } else {
                this.currentSnackbar = null;
            }
        }

    }

    private boolean cancelSnackbarLocked(SnackInfoManager.SnackbarRecord record, int event) {
        SnackInfoManager.Callback callback = record.callback.get();
        if (callback != null) {
            this.handler.removeCallbacksAndMessages(record);
            callback.dismiss(event);
            return true;
        } else {
            return false;
        }
    }

    private boolean isCurrentSnackbarLocked(SnackInfoManager.Callback callback) {
        return this.currentSnackbar != null && this.currentSnackbar.isSnackbar(callback);
    }

    private boolean isNextSnackbarLocked(SnackInfoManager.Callback callback) {
        return this.nextSnackbar != null && this.nextSnackbar.isSnackbar(callback);
    }

    private void scheduleTimeoutLocked(SnackInfoManager.SnackbarRecord r) {
        if (r.duration !=  SnackInfo.LENGTH_INDEFINITE) {
            int durationMs = LONG_DURATION_MS;
            if (r.duration > 0) {
                durationMs = r.duration;
            } else if (r.duration == SnackInfo.LENGTH_SHORT) {
                durationMs = SHORT_DURATION_MS;
            }

            this.handler.removeCallbacksAndMessages(r);
            this.handler.sendMessageDelayed(Message.obtain(this.handler, MSG_TIMEOUT, r), (long) durationMs);
        }
    }

    void handleTimeout(SnackInfoManager.SnackbarRecord record) {
        synchronized (lock) {
            if (currentSnackbar == record || nextSnackbar == record) {
                this.cancelSnackbarLocked(record, SnackInfo.Callback.DISMISS_EVENT_TIMEOUT);
            }

        }
    }

    private static class SnackbarRecord {
        final WeakReference<Callback> callback;
        int duration;
        boolean paused;

        SnackbarRecord(int duration, SnackInfoManager.Callback callback) {
            this.callback = new WeakReference(callback);
            this.duration = duration;
        }

        boolean isSnackbar(SnackInfoManager.Callback callback) {
            return callback != null && this.callback.get() == callback;
        }
    }

    interface Callback {
        void show();

        void dismiss(int event);
    }
}
