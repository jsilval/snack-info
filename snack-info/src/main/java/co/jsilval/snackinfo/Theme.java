/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package co.jsilval.snackinfo;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RoundRectShape;
import androidx.annotation.ColorInt;

public class Theme {
    private static float density = 1.0F;
    private static Theme instance;

    private static Theme getInstance(Context context) {
        if (instance == null) {
            instance = new Theme(context);
        }

        return instance;
    }

    private Theme(Context context) {
        this.checkDisplaySize(context);
    }

    private void checkDisplaySize(Context context) {
        try {
            density = context.getResources().getDisplayMetrics().density;
        } catch (Exception var6) {
            // ignore
        }

    }

    private int dp(float value) {
        return value == 0.0F ? 0 : (int) Math.ceil((double) (density * value));
    }

    private static Drawable createRoundRectDrawable(Context context, int topRightRad, int topLeftRad,
                                                    int bottomRightRad, int bottomLeftRad, @ColorInt int color) {

        RoundRectShape roundRectShape = new RoundRectShape(new float[]{
                (float) Theme.getInstance(context).dp((float) topRightRad),
                (float) Theme.getInstance(context).dp((float) topRightRad),
                (float) Theme.getInstance(context).dp((float) topLeftRad),
                (float) Theme.getInstance(context).dp((float) topLeftRad),
                (float) Theme.getInstance(context).dp((float) bottomRightRad),
                (float) Theme.getInstance(context).dp((float) bottomRightRad),
                (float) Theme.getInstance(context).dp((float) bottomLeftRad),
                (float) Theme.getInstance(context).dp((float) bottomLeftRad)},
                null, null);
        ShapeDrawable drawable = new ShapeDrawable(roundRectShape);
        drawable.getPaint().setColor(color);
        return drawable;
    }

    public static Drawable createRoundRectDrawable(Context context, int rad, @ColorInt int color) {
        return createRoundRectDrawable(context, rad, rad, rad, rad, color);
    }
}
